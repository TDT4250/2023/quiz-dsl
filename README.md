# Quiz DSL

This project contains a DSL for creating quizes, composed of a metamodel, Xtext editor, and code generator.

The project was craeted by [Hallvard Trætteberg](https://github.com/hallvard) as part of the TDT4250 course at NTNU/IDI. Another version of the same project can be found [here](https://git.it.ntnu.no/projects/TDT4250/repos/quiz/).

The project was created with an older version of Eclipse and it might not work with newer versions of Eclipse. **The project has been verified to work on Eclipse 2020-06**. 
